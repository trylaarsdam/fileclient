cloud_client

Sends the file `microcode.o` over TCP to `trek.thewcl.com:9000` and stores it on `trek.thewcl.com` until another file is sent (in which case the other file is overwritten).