const express = require('express');
const fs = require('fs');
const net = require('net');
var md5 = require('md5');


let fileBuffer;
let chunks = [];



fs.readFile('./microcode.o', function (err, data) {
    fileBuffer = data;
    sendRequest();
    //var fullData = Buffer.alloc(1 + fileBuffer.length + md5(fileBuffer).length);
    //fullData.write(String.fromCharCode(fileBuffer.length));
    //fullData.write(fileBuffer + md5(fileBuffer));
    //console.log(fullData);
    //client.connect(9000, 'localhost', function () {
    //    client.write(fullData);
    //});
})
/*var client = new net.Socket();
client.on('data', function (data) {
    if (data == fileBuffer) {
    }
    client.destroy(); // kill client after server's response
});

client.on('close', function () {
});*/


/////////////////////////////////
//        HTTPS REQUESTS       //
/////////////////////////////////

const https = require('https');
const http = require('http');

function sendRequest(){
    console.log(fileBuffer);
    var data = JSON.stringify({
        time: new Date(),
        md5: md5(fileBuffer.toString()),
        data: fileBuffer
    })
    
    const serverOptions = {
        hostname: 'trekdev.thewcl.com',
        port: 443,
        path: '/api/desktop/up',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': data.length
        }
    }
    
    const req = https.request(serverOptions, res => {
    
        res.on('data', d => {
            process.stdout.write(d)
            req.end()
        })
    })

    req.on('error', error => {
        console.error(error)
        req.end()
    })

    console.log(data);
    req.write(data);
}

